"""Utility Functions for the Transformer model.

Credits:
    https://github.com/tensorflow/examples/blob/master/community/en/position_encoding.ipynb
"""
from typing import Any, Optional, Union

import numpy as np
import tensorflow as tf

# Types.
Array = Union[np.ndarray, Any]
Tensor = Union[tf.Tensor, np.ndarray, Any]


################################################################################################
# +--------------------------------------------------------------------------------------------+
# | Positional Encoding
# +--------------------------------------------------------------------------------------------+
################################################################################################
def get_angles(pos: Array, i: Array, d_model: int):
    angle_rates = 1 / np.power(10000, (2 * (i//2)) / np.float32(d_model))
    return pos * angle_rates


def positional_encoding(position: int, d_model: int):
    angle_rads = get_angles(np.arange(position)[:, np.newaxis],
                            np.arange(d_model)[np.newaxis, :],
                            d_model)

    # Apply sin to even indices in the array; 2i
    angle_rads[:, 0::2] = np.sin(angle_rads[:, 0::2])

    # Apply cos to odd indices in the array; 2i + 1
    angle_rads[:, 1::2] = np.cos(angle_rads[:, 1::2])

    pos_encoding = angle_rads[np.newaxis, ...]

    return tf.cast(pos_encoding, dtype=tf.float32)


################################################################################################
# +--------------------------------------------------------------------------------------------+
# | Masking.
# +--------------------------------------------------------------------------------------------+
################################################################################################
def create_padding_mask(seq: Array):
    """Mask all the pad tokens in the batch of sequence.

    Note:
        It ensures that the model does not treat padding as the input.
        The mask indicates where pad value `0` is present: it outputs a
        `1` at those locations, and a `0` otherwise.

    Arguments:
        seq {tf.Tensor} -- Sequence to be paded.

    Examples:
        ```python
        >>> import tensorflow as tf
        >>>
        >>> x = tf.constant([[7, 7, 0, 0, 1], [1, 2, 3, 0, 0], [0, 0, 0, 4, 5]])
        >>>
        >>> create_padding_mask(x)
        <tf.Tensor: shape=(3, 1, 1, 5), dtype=float32, numpy=
        array([[[[0., 0., 1., 1., 0.]]],

               [[[0., 0., 0., 1., 1.]]],

               [[[1., 1., 1., 0., 0.]]]], dtype=float32)>
        ```

    Returns:
        tf.Tensor -- A padded sequence where the padding is replaced with `1`s.
            shape = `(batch_size, 1, 1, seq_len)`.
    """
    seq = tf.cast(tf.math.equal(seq, 0), tf.float32)

    # Add extra dimensions to add the padding to the
    # attention logits.
    return seq[:, tf.newaxis, tf.newaxis, :]  # (batch_size, 1, 1, seq_len)


def create_lookahead_mask(size: int):
    """Look-ahead mask to mask the future tokens in a sequence.

    Arguments:
        size {int} -- Sequence length to mask.

    Returns:
        tf.Tensor -- A masked tensor `0` or `1` with shape `(seq_len, seq_len)`.
    """
    mask = 1 - tf.linalg.band_part(tf.ones((size, size)), -1, 0)
    return mask  # (seq_len, seq_len)


################################################################################################
# +--------------------------------------------------------------------------------------------+
# | Scaled Dot-Product Attention.
# +--------------------------------------------------------------------------------------------+
################################################################################################
def scaled_doct_product_attention(q: Tensor, k: Tensor, v: Tensor, mask: Optional[Tensor] = None):
    """Calculate the attention weights.

    Note:
        `q`, `k`, `v` must have matching leading dimensions.
        `k`, `v` must have matching penultimate dimension, i.e.: `seq_len_k == seq_len_v`.
        The mask has different shapes depending on its type (padding or look ahead)
        but it must be broadcastable for addition.

    Arguments:
        q {tf.Tensor} -- Query. `q.shape == (..., seq_len_q. depth)`
        k {tf.Tensor} -- Key. `k.shape == (..., seq_len_k, depth)`
        v {tf.Tensor} -- Value. `v.shape == (..., seq_len_v, depth_v)`
        mask {tf.Tensor} -- A float tensor with shape broadcastable to
            `(..., seq_len_q, seq_len_k)`. Defaults to None.

    Returns:
        `(output, attention_weights)`
    """

    # (..., seq_len_q, seq_len_k)
    mat_mul_qk = tf.matmul(q, k, transpose_b=True)

    # Scale mat_mul_qk
    dk = tf.cast(tf.shape(k)[-1], tf.float32)
    scaled_attn_logits = mat_mul_qk / tf.math.sqrt(dk)

    # Add the mask to the scaled tensor.
    if mask is not None:
        scaled_attn_logits += (mask * -1e9)

    # Softmax is normalized on the last axis `(seq_len_k)` so that the scores
    # add up to 1.
    # (..., seq_len_q, seq_len_k)
    attn_weights = tf.nn.softmax(scaled_attn_logits, axis=-1)

    output = tf.matmul(attn_weights, v)  # (..., seq_len_q, depth_v)

    return output, attn_weights
