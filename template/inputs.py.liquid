#!/usr/bin/env python

# Copyright 2017 Google Inc. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import argparse
import json
import multiprocessing

from typing import Any, Dict, Iterable, Optional
from typing import MutableMapping, Sequence, Union

import tensorflow as tf

from {{project}}.config import Log
from {{project}} import metadata, featurizer


################################################################################################
# +--------------------------------------------------------------------------------------------+
# | YOU NEED NOT TO CHANGE THESE FUNCTIONS TO PARSE THE INPUT RECORDS.
# +--------------------------------------------------------------------------------------------+
################################################################################################
def parse_csv(csv_row: Any, is_serving: bool = False):
    """Takes a batch of csv rows and returns a dict of rank-2 tensors.

    Takes a rank-2 tensor and converts it into rank-2 tensor, with respect to
    its data type (inferred from the metadata)

    Args:
        csv_row: rank-2 tensor of type string (csv rows)
        is_serving: boolean to indicate whether this function is called during
        serving or training since the serving csv_row input is different than
        the training input (i.e., no target column)
    Returns:
        rank-2 tensors' dictionary of the correct data type
    """

    if is_serving:
        column_names = metadata.SERVING_COLUMNS
        defaults = metadata.SERVING_DEFAULTS
    else:
        column_names = metadata.HEADER
        defaults = metadata.HEADER_DEFAULTS

    defaults = [[default] for default in defaults]
    columns = tf.io.decode_csv(csv_row, record_defaults=defaults)

    features: Dict[str, Any] = dict(zip(column_names, columns))

    return features


def parse_tf_example(example_proto: Any, is_serving: bool = False):
    """Takes a batch of string input tensors (example proto) and returns a dict of rank-2 tensors.

    Takes a rank-2 tensor and converts it into a dictionary of rank-2 tensor, with respect to its data type
    (inferred from the  metadata)

    Args:
        example_proto: rank-2 tensor of type string (example proto)
        is_serving: boolean to indicate whether this function is called during serving or training
        since the serving csv_row input is different than the training input (i.e., no target column)
    Returns:
        rank-2 tensors' dictionary of the correct data type
    """

    feature_spec: MutableMapping[str, Any] = {}

    for feature_name in metadata.INPUT_FEATURE_NAMES:
        feature_spec[feature_name] = tf.FixedLenFeature(
            shape=1, dtype=tf.float32)

    for feature_name in metadata.INPUT_CATEGORICAL_FEATURE_NAMES:
        feature_spec[feature_name] = tf.FixedLenFeature(
            shape=1, dtype=tf.string)

    if not is_serving:
        feature_spec[metadata.TARGET_NAME] = tf.FixedLenFeature(
            shape=(), dtype=tf.float32)  # dtype could be tf.float32 for regression

    parsed_features = tf.io.parse_example(serialized=example_proto,
                                          features=feature_spec)

    return parsed_features


def process_text(texts: Sequence[str], tokenizer: Optional[tf.keras.preprocessing.text.Tokenizer] = None,
                 num_words: Optional[int] = None, maxlen: Optional[int] = None, dtype: str = 'int32',
                 padding: str = 'pre', truncating: str = 'pre', pad_value: float = 0.0):
    """Tokenize (string) text sequence into numeric tokens.

    Args:
        texts (List[str]): Raw text sequence to tokenize.
        tokenizer (tf.keras.processing.text.Tokenizer, optional): A keras `Tokenizer` object.
            If None is provided, one is created by default. Defaults to None.
        num_words (int, optional): Maximum number of words to keep. Defaults to None.
        maxlen (int, optional): Maximum sequence length for longer and shorter sequences.
            Defaults to None.
        dtype (str, optional): Data type of target Tensor. Defaults to 'int32'.
        padding (str, optional): Pad shorter sequences 'pre' or 'post'. Defaults to 'pre'.
        truncating (str, optional): Truncate longer sequences at the end of begining.
            'pre' or 'post'. Defaults to 'pre'.
        pad_value (float, optional): Value to pad shorter sequences with. Defaults to 0.0.

    Returns:
        Tuple[tf.Tensor, tf.keras.preprocessing.text.Tokenizer]: A `tf.Tensor` object of same shape
            as input `texts` and it's appropriate `Tokenizer` object.
    """
    if not tokenizer:
        # Create a new Tokenizer object if `None` is provided.
        tokenizer = tf.keras.preprocessing.text.Tokenizer(num_words=num_words)
        # Train tokenizer on text sequence.
        tokenizer.fit_on_texts(texts)

    # Converts text to numeric tokens.
    tensor = tokenizer.texts_to_sequences(texts)
    # Pad shorter sequences & truncates longer sequences.
    tensor = tf.keras.preprocessing.sequence.pad_sequences(tensor, maxlen=maxlen,
                                                           dtype=dtype, padding=padding,
                                                           truncating=truncating, value=pad_value)

    return tensor, tokenizer


################################################################################################
# +--------------------------------------------------------------------------------------------+
# | YOU MAY IMPLEMENT THIS FUNCTION FOR CUSTOM FEATURE ENGINEERING.
# +--------------------------------------------------------------------------------------------+
################################################################################################
def process_features(features: MutableMapping[str, Union[tf.Tensor, Any]]):
    """Use to implement custom feature engineering logic, e.g. polynomial expansion, etc.

    Default behaviour is to return the original feature tensors dictionary as-is.

    Args:
        features: {string:tensors} - dictionary of feature tensors.

    Examples:
        examples - given:
        'x' and 'y' are two numeric features:
        'alpha' and 'beta' are two categorical features

        create new features using custom logic
        features['x_2'] = tf.pow(features['x'],2)
        features['y_2'] = tf.pow(features['y'], 2)
        features['xy'] = features['x'] * features['y']
        features['sin_x'] = tf.sin(features['x'])
        features['cos_y'] = tf.cos(features['x'])
        features['log_xy'] = tf.log(features['xy'])
        features['sqrt_xy'] = tf.sqrt(features['xy'])

        create boolean flags
        features['x_grt_y'] = tf.cast(features['x'] > features['y'], tf.int32)
        features['alpha_eq_beta'] = features['alpha'] == features['beta']

        add created features to metadata (if not already defined in metadata.py)
        CONSTRUCTED_NUMERIC_FEATURE_NAMES += ['x_2', 'y_2', 'xy', ....]
        CONSTRUCTED_CATEGORICAL_FEATURE_NAMES_WITH_IDENTITY['x_grt_y'] = 2
        CONSTRUCTED_CATEGORICAL_FEATURE_NAMES_WITH_IDENTITY['alpha_eq_beta'] = 2

    Returns:
        {string:tensors}: extended feature tensors dictionary
    """

    return features


def get_features_target_tuple(features: Dict[str, tf.Tensor]):
    """Get a tuple of input feature tensors and target feature tensor.

    Args:
        features: {string:tensors} - dictionary of feature tensors.

    Returns:
          {string:tensors}, {tensor} -  input feature tensor dictionary and target feature tensor
    """

    # # For Classification tasks: get target tensor for each labels.
    # target = {t: features.pop(t)
    #           for t in metadata.TARGET_LABELS}

    # For regression tasks: get target tensor (& expand it's dimension).
    target = {
        metadata.TARGET_NAME:  tf.expand_dims(
            features.pop(metadata.TARGET_NAME),
            axis=0
        )
    }

    return features, target


################################################################################################
# +--------------------------------------------------------------------------------------------+
# | YOU NEED NOT TO CHANGE THIS FUNCTION TO READ DATA FILES.
# +--------------------------------------------------------------------------------------------+
################################################################################################
{%if runtime == "tensorflow" %}
def generate_input_fn(file_names_pattern: Union[str, Iterable[str]],
                      compression_type: Optional[str] = None,
                      file_encoding: str = 'csv',
                      mode: str = tf.estimator.ModeKeys.TRAIN,
                      num_epochs: int = 1,
                      batch_size: int = 200,
                      multi_threading: bool = True,
                      batch_by_context: bool = False):
    """Generates an input function for reading training and evaluation data file(s).
    This uses the tf.data APIs.

    Args:
        file_names_pattern: [str] - file name or file name patterns from which to read the data.
        compression_type: [str] - Type of compression files for the `file_names_pattern`.
            A `tf.string` scalar evaluating to one of `""` (no compression), `"ZLIB"`, or `"GZIP"`.
        file_encoding: type of the text files. Can be 'csv' or 'tfrecords'
        mode: tf.estimator.ModeKeys - either TRAIN or EVAL.
            Used to determine whether or not to randomize the order of data.
        skip_header_lines: int set to non-zero in order to skip header lines in CSV files.
        num_epochs: int - how many times through to read the data.
          If None will loop through data indefinitely
        batch_size: int - first dimension size of the Tensors returned by input_fn
        multi_threading: boolean - indicator to use multi-threading or not

    Returns:
        A function () -> callable[None, Union[tf.data.Dataset, Tuple[Any, Any]]]
            (features, indices) where features is a dictionary of Tensors,
            and indices is a single Tensor of label indices.
    """
    def _input_fn():

        shuffle = mode == tf.estimator.ModeKeys.TRAIN
        num_threads = multiprocessing.cpu_count() if multi_threading else 1
        buffer_size = 2 * batch_size + 1
        file_names = tf.gfile.io.glob(file_names_pattern)

        if file_encoding == 'csv':
            def read_csv_lines(filename):
                dataset = tf.data.TextLineDataset(
                    filename,
                    compression_type
                ).skip(1).map(parse_csv, num_threads)

                if batch_by_context:
                    dataset = dataset.take(batch_size)
                return dataset

            dataset = (
                tf.data.Dataset.from_tensor_slices(file_names)
                .interleave(read_csv_lines,
                            cycle_length=num_threads,
                            block_length=1,
                            num_parallel_calls=tf.data.experimental.AUTOTUNE)
            )

        else:
            dataset = tf.data.TFRecordDataset(filenames=file_names)
            dataset = dataset.batch(batch_size)
            dataset = dataset.map(
                lambda tf_examples: parse_tf_example(tf_examples,
                                                     is_serving=False),
                num_parallel_calls=num_threads
            )

        dataset = dataset.map(lambda features: get_features_target_tuple(features),
                              num_parallel_calls=num_threads)
        dataset = dataset.map(lambda features, targets: (process_features(features), targets),
                              num_parallel_calls=num_threads)

        if shuffle:
            dataset = dataset.shuffle(buffer_size)

        dataset = dataset.prefetch(tf.data.experimental.AUTOTUNE)
        dataset = dataset.batch(batch_size)
        # dataset = dataset.padded_batch(batch_size, drop_remainder=True)
        dataset = dataset.repeat(num_epochs)

        return dataset

    return _input_fn
{% else %}
def load_data(file_names_pattern,
              compression_type=None,
              file_encoding='csv',
              mode=tf.estimator.ModeKeys.TRAIN,
              skip_header_lines=0,
              num_epochs=1,
              batch_size=200,
              multi_threading=True,
              batch_by_context=False):
    """Generates an input function for reading training and evaluation data file(s).
    This uses the tf.data APIs.

    Args:
        file_names_pattern: [str] - file name or file name patterns from which to read the data.
        mode: tf.estimator.ModeKeys - either TRAIN or EVAL.
            Used to determine whether or not to randomize the order of data.
        file_encoding: type of the text files. Can be 'csv' or 'tfrecords'
        skip_header_lines: int set to non-zero in order to skip header lines in CSV files.
        num_epochs: int - how many times through to read the data.
          If None will loop through data indefinitely
        batch_size: int - first dimension size of the Tensors returned by input_fn
        multi_threading: boolean - indicator to use multi-threading or not
    Returns:
        A function () -> (features, indices) where features is a dictionary of
          Tensors, and indices is a single Tensor of label indices.
    """

    shuffle = mode == tf.estimator.ModeKeys.TRAIN

    num_threads = multiprocessing.cpu_count() if multi_threading else 1

    buffer_size = 2 * batch_size + 1

    file_names = tf.gfile.Glob(file_names_pattern)

    if file_encoding == 'csv':
        def read_csv_lines(filename):
            dataset = tf.data.TextLineDataset(
                filename, compression_type=compression_type
            ).skip(1).shuffle(4 * batch_size).map(parse_csv, num_threads)

            if batch_by_context:
                dataset = dataset.take(batch_size)
            return dataset

        dataset = (
            tf.data.Dataset.from_tensor_slices(file_names)
            .shuffle(1000)
            .interleave(read_csv_lines,
                        cycle_length=num_threads,
                        block_length=1)
        )

    else:
        dataset = tf.data.TFRecordDataset(filenames=file_names)
        dataset = dataset.batch(batch_size)
        dataset = dataset.map(
            lambda tf_examples: parse_tf_example(tf_examples),
            num_parallel_calls=num_threads)

    dataset = dataset.map(lambda features: get_features_target_tuple(features),
                          num_parallel_calls=num_threads)
    dataset = dataset.map(lambda features, targets: (process_features(features), targets),
                          num_parallel_calls=num_threads)

    if shuffle:
        dataset = dataset.shuffle(buffer_size)

    dataset = dataset.prefetch(buffer_size)
    dataset = dataset.repeat(num_epochs)

    iterator = dataset.make_one_shot_iterator()
    features, labels = iterator.get_next()

    # with tf.control_dependencies([features['']]):
    feature_columns = featurizer.create_feature_columns()

    # Model inputs features
    description_embedding = feature_columns['description_embedding']
    inputs_ = tf.feature_column.input_layer(features,
                                            description_embedding)
    outputs_ = labels['price']

    # return features, labels
    # return dataset
    return {'features': inputs_, 'labels': outputs_}

{% endif %}

################################################################################################
# +--------------------------------------------------------------------------------------------+
# | YOU MAY CHANGE THIS FUNCTION TO LOAD YOUR NUMERIC COLUMN STATS.
# +--------------------------------------------------------------------------------------------+
################################################################################################
def load_feature_stats(feature_stats_file: Optional[str] = None):
    """
    Load numeric column pre-computed statistics (mean, stdv, min, max, etc.)
    in order to be used for scaling/stretching numeric columns.

    In practice, the statistics of large datasets are computed prior to model training,
    using dataflow (beam), dataproc (spark), BigQuery, etc.

    The stats are then saved to gcs location. The location is passed to package
    in the --feature-stats-file argument. However, it can be a local path as well.

    Returns:
        json object with the following schema: stats['feature_name']['state_name']
    """

    feature_stats: Optional[Dict[str, Any]] = None

    try:
        if feature_stats_file is not None and tf.io.gfile.exists(feature_stats_file):
            with tf.io.gfile.GFile(feature_stats_file) as file:
                content = file.read()
            feature_stats = json.loads(content)
            Log.info('Feature stats were successfully loaded from local file...')
        else:
            Log.warn(('Feature stats file not found. numerical'
                      ' columns will not be normalized...'))
    except:
        Log.exception(('Could not load feature stats. numerical'
                       ' columns will not be normalized...'))

    return feature_stats


################################################################################################
# +--------------------------------------------------------------------------------------------+
# | SERVING FUNCTIONS YOU NEED NOT TO CHANGE THE FOLLOWING PART.
# +--------------------------------------------------------------------------------------------+
################################################################################################
class ServingFunctionGenerator(object):

    def __init__(self, hyper_params: argparse.Namespace):
        self.hyper_params = hyper_params

    def get_serving_function(self, serving_function_type: str):
        serving_function_dict = {
            'JSON': self.json_serving_input_fn,
            'EXAMPLE': self.example_serving_input_fn,
            'CSV': self.csv_serving_input_fn
        }
        return serving_function_dict[serving_function_type]

    def json_serving_input_fn(self):
        def create_feature_column(name: str, default_value: Union[int, float, str]):
            if isinstance(default_value, int):
                return tf.compat.v1.placeholder(shape=[1, None],
                                                dtype=tf.int32,
                                                name=name)
            elif isinstance(default_value, float):
                return tf.compat.v1.placeholder(shape=[1, None],
                                                dtype=tf.float32,
                                                name=name)
            elif isinstance(default_value, str):
                return tf.compat.v1.placeholder(shape=[1, None],
                                                dtype=tf.string,
                                                name=name)
            raise ValueError((f'Unrecognized type for {default_value}'))

        inputs = dict([(name, create_feature_column(name, default_value))
                       for name, default_value in zip(metadata.JSON_SERVING_COLUMNS,
                                                      metadata.JSON_SERVING_DEFAULTS)
                       ])

        features: MutableMapping[str, Union[tf.Tensor, Any]] = {}
        for column_name in inputs:
            features[column_name] = tf.squeeze(inputs[column_name], [0])

        return tf.estimator.export.ServingInputReceiver(
            features=process_features(features, None, is_serving=True),
            receiver_tensors=inputs)

    def csv_serving_input_fn(self):
        csv_row = tf.compat.v1.placeholder(
            shape=[None],
            dtype=tf.string,
            name="csv_row"
        )

        features = parse_csv(csv_row, is_serving=True)

        unused_features = list({metadata.TARGET_NAME})

        # Remove unused columns (if any)
        for column in unused_features:
            features.pop(column, None)

        # features = process_features(features, None, is_serving=True)
        features = process_features(features)

        return tf.estimator.export.ServingInputReceiver(
            features=features,
            receiver_tensors={'csv_row': csv_row})

    def example_serving_input_fn(self):
        feature_columns = featurizer.create_feature_columns(self.hyper_params)
        input_feature_columns = [
            feature_columns[feature_name]
            for feature_name in metadata.INPUT_FEATURE_NAMES
        ]

        example_bytestring = tf.compat.v1.placeholder(
            shape=[None],
            dtype=tf.string,
        )

        feature_scalars = tf.parse_example(
            example_bytestring,
            tf.feature_column.make_parse_example_spec(input_feature_columns)
        )

        features = {
            key: tf.expand_dims(tensor, -1)
            for key, tensor in feature_scalars.iteritems()
        }

        return tf.estimator.export.ServingInputReceiver(
            features=process_features(features),
            receiver_tensors={
                'example_proto': example_bytestring
            })
