"""Starting point.

Credits:

"""
import argparse

import tensorflow as tf

from {{project}} import inputs, model
from {{project}}.config import Log, FS


def initialize_hyper_params(parser: argparse.ArgumentParser):
    # +--------------------------------------------------------------------------------------------+
    # | Data files.
    # +--------------------------------------------------------------------------------------------+
    parser.add_argument('--train-files', required=True, nargs='+',
                        help='GCS or local paths to training data')

    parser.add_argument('--eval-files', required=True, nargs='+',
                        help='GCS or local paths to evaluation data')

    parser.add_argument('--compression-type', type=str, default=None,
                        choices=['GZIP', 'ZLIB'],
                        help='Compression type of data files')

    parser.add_argument('--feature-stats-file', nargs='+', default=None,
                        help='GCS or local paths to feature statistics json file')

    # +--------------------------------------------------------------------------------------------+
    # | Experiment arguments -- training
    # +--------------------------------------------------------------------------------------------+
    parser.add_argument('--train-steps', type=int, default=1e7,
                        help=('Steps to run the training job for. If `--num-epochs` and '
                              '`--train-size` are not specified, this must be. Otherwise the training'
                              ' job will run indefinitely. if `--num-epochs` and `--train-size` are specified,'
                              ' then `--train-steps` will be: `(train-size/train-batch-size) * num-epochs`.'))

    parser.add_argument('--train-batch-size', type=int, default=512,
                        help='Batch size for each training step')

    parser.add_argument('--train-size', type=int, default=None,
                        help='Size of training set (instance count)')

    parser.add_argument('--num-epochs', type=int, default=None,
                        help=('Maximum number of training data epochs on which to train.'
                              'If both `--train-size` and `--num-epochs` are specified,'
                              '`--train-steps` will be: `(train-size/train-batch-size) * num-epochs`.'))

    # +--------------------------------------------------------------------------------------------+
    # | Experiment arguments -- evaluation
    # +--------------------------------------------------------------------------------------------+
    parser.add_argument('--eval-every-secs', type=int, default=120,
                        help='How long to wait before running the next evaluation')
    parser.add_argument('--eval-steps', type=int, default=None,
                        help=('Number of steps to run evaluation for at each checkpoint.'
                              'Set to None to evaluate on the whole evaluation data'))
    parser.add_argument('--eval-batch-size', type=int, default=512,
                        help='Batch size for evaluation steps')

    # +--------------------------------------------------------------------------------------------+
    # | Saved model.
    # +--------------------------------------------------------------------------------------------+
    parser.add_argument('--job-dir', type=str, required=True,
                        help='GCS location to write checkpoints and export models')

    parser.add_argument('--reuse-job-dir', default=False, action='store_true',
                        help=('Flag to decide if the model checkpoint should'
                              'be re-used from the job-dir. If False then the'
                              'job-dir will be deleted'))

    parser.add_argument('--model-dir', type=str, default=FS.MODELS_DIR,
                        help='GCS location to write saved model')

    parser.add_argument('--export-format', type=str, default='CSV',
                        choices=['JSON', 'CSV', 'EXAMPLE'],
                        help='The input format of the exported SavedModel binary')

    parser.add_argument('--warm-start-ckpt', type=str, default=None,
                        help='The checkpoint to warm start variables from')

    parser.add_argument('--checkpoint-path', type=str, default=FS.CHKPT_DIR,
                        help='Path to the model checkpoint directory.')

    # +--------------------------------------------------------------------------------------------+
    # | Logging.
    # +--------------------------------------------------------------------------------------------+
    parser.add_argument('--verbosity', type=str, default='INFO',
                        choices=['DEBUG', 'ERROR',
                                 'FATAL', 'INFO', 'WARN'],
                        help='Verbosity & log level.')

    parser.add_argument('--seed', type=int, default=42,
                        help='Set random seed for estimator')

    parser.add_argument('--log-steps', type=int, default=10,
                        help='Log every n iteration.')

    parser.add_argument('--checkpoint-every-sec', type=int, default=120,
                        help='time between checkpoints')

    parser.add_argument('--num-checkpoint-to-save', type=int, default=3,
                        help='Number of checkpoints to save')

    # +--------------------------------------------------------------------------------------------+
    # | Model arguments.
    # +--------------------------------------------------------------------------------------------+
    parser.add_argument('--mode', type=str, default='train',
                        choices=['train', 'eval', 'infer'],
                        help='Mode to run the model.')

    parser.add_argument('--learning-rate', type=float, default=0.01,
                        help="Learning rate value for the optimizers")

    parser.add_argument('--dropout-rate', type=float, default=0.1,
                        help='Rate to randomly turn off neurons during training.')

    parser.add_argument('--no-use-MLS', default=True,
                        dest='use_MLS', action="store_false",
                        help="Do not use MLS layers")

    args: argparse.Namespace = parser.parse_args()

    # Update model & job dirs.
    FS.JOB_DIR = args.job_dir
    FS.MODELS_DIR = args.model_dir = f"{args.job_dir.rstrip('/')}/models"
    FS.CKPT_DIR = args.checkpoint_path or f'{FS.JOB_DIR}/ckpt'

    return args

{% if runtime == "tensorflow" %}
def run_experiment(args: argparse.Namespace):
    # Training input function.
    train_input_fn = inputs.generate_input_fn(
        file_names_pattern=args.train_files,
        compression_type=args.compression_type,
        mode=tf.estimator.ModeKeys.TRAIN,
        num_epochs=args.num_epochs,
        batch_size=args.train_batch_size,
        batch_by_context=args.use_MLS
    )

    # Evaluation input function.
    eval_input_fn = inputs.generate_input_fn(
        file_names_pattern=args.eval_files,
        compression_type=args.compression_type,
        mode=tf.estimator.ModeKeys.EVAL,
        batch_size=args.eval_batch_size,
        batch_by_context=args.use_MLS
    )

    serving_fn = inputs.ServingFunctionGenerator(args)
    serving_input_receiver_fn = serving_fn.get_serving_function(
        serving_function_type=args.export_format
    )
    exporter = tf.estimator.FinalExporter('estimator',
                                          serving_input_receiver_fn,
                                          as_text=True)

    # Compute the number of training steps based on num_epoch, train_size, and train_batch_size
    if args.train_size is not None and args.num_epochs is not None:
        train_steps = int((args.train_size / args.train_batch_size)
                          * args.num_epochs)
    else:
        train_steps = args.train_steps

    train_spec = tf.estimator.TrainSpec(
        train_input_fn,
        max_steps=train_steps
    )

    eval_spec = tf.estimator.EvalSpec(
        eval_input_fn,
        exporters=[exporter],
        throttle_secs=args.eval_every_secs,
    )

    estimator = model.create_estimator(args)

    # Train & evaluate estimator.
    tf.estimator.train_and_evaluate(estimator, train_spec, eval_spec)

    # Export the model as a saved model.
    estimator.export_saved_model(export_dir_base=args.job_dir,
                                 serving_input_receiver_fn=serving_input_receiver_fn)
    Log.info(f'Exported model to {args.job_dir}')

{% else %}

def run_experiment(args):

    # Compute the number of training steps based on num_epoch, train_size, and train_batch_size
    if args.train_size is not None and args.num_epochs is not None:
        train_steps = int((args.train_size / args.train_batch_size)
                          * args.num_epochs)
    else:
        train_steps = args.train_steps

    # Training input function.
    train_data = inputs.load_data(
        file_names_pattern=args.train_files,
        compression_type=args.compression_type,
        mode=tf.estimator.ModeKeys.TRAIN,
        num_epochs=args.num_epochs,
        batch_size=args.train_batch_size,
        batch_by_context=args.use_MLS
    )

    # Evaluation input function.
    eval_data = inputs.load_data(
        file_names_pattern=args.eval_files,
        compression_type=args.compression_type,
        mode=tf.estimator.ModeKeys.EVAL,
        batch_size=args.eval_batch_size,
        batch_by_context=args.use_MLS
    )

    Log.debug('Train: {}'.format(train_data))
    Log.debug('Eval: {}'.format(eval_data))

    # Setup Learning Rate decay.
    lr_decay_cb = tf.keras.callbacks.LearningRateScheduler(
        lambda epoch: args.learning_rate + 0.02 * (0.5 ** (1 + epoch)),
        verbose=True
    )

    # Setup TensorBoard callback.
    tensorboard_cb = tf.keras.callbacks.TensorBoard(
        args.job_dir,
        histogram_freq=1
    )

    # (Compiled) Keras model.
    keras_model = model.create_keras_model(vars(args))

    # Train model
    keras_model.fit(
        x=train_data['features'], y=train_data['labels'],
        steps_per_epoch=train_steps,
        epochs=args.num_epochs,
        validation_data=(eval_data['features'], eval_data['labels']),
        validation_steps=1, verbose=1,
        callbacks=[lr_decay_cb, tensorboard_cb]
    )

    # Save to a TF 2.x format.
    tf.keras.models.save_model(keras_model, args.job_dir, save_format="tf")
    Log.info('Model exported to: {}'.format(args.job_dir))
{% endif %}

def main(args: argparse.Namespace):
    Log.info('Experiment started...')

    run_experiment(args)

    Log.info('Experiment finished...')


if __name__ == '__main__':

    # TODO: Change the epilog & description for the arg parser.
    parser = argparse.ArgumentParser(epilog='Text following the argument descriptions.',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter,
                                     description='A description of what the program does.')

    args: argparse.Namespace = initialize_hyper_params(parser)
    main(args)
