import os
from googleapiclient import discovery
from oauth2client.client import GoogleCredentials


# TODO: Change to GCP project where the AI Platform model is deployed
GCP_PROJECT = ''

# TODO: Change to the deployed AI Platform model
MODEL_NAME = ''

# If None, the default version will be used
MODEL_VERSION = None


def predict(instances):
    """Use a deployed model to AI Platform to perform prediction

    Args:
        instances(List[str, Any]): list of json, csv, or tf.example objects, based on the serving function called.

    Returns:
        response - dictionary. If no error, response will include an item with 'predictions' key
    """

    credentials = GoogleCredentials.get_application_default()

    service = discovery.build('ml', 'v1', credentials=credentials)
    model_url = f'projects/{GCP_PROJECT}/models/{MODEL_NAME}'

    if MODEL_VERSION is not None:
        model_url += f'/versions/{MODEL_VERSION}'

    request_data = {
        'instances': instances
    }

    response = service.projects().predict(
        body=request_data,
        name=model_url
    ).execute()

    return response
