import sys
import argparse

from typing import Any, Dict, List, Optional, Union

import tensorflow as tf
from tensorflow.python.eager.function import ConcreteFunction
from tensorflow.python.training.tracking.tracking import AutoTrackable


Tensor = Union[tf.Tensor, List[str], Any]


def predict(inputs: Tensor, job_dir: str, *,
            feeds: Optional[Union[str, List[str]]] = None,
            fetches: Optional[Union[str, List[str]]] = None,
            structured_outputs: bool = True,
            tags: str = 'serving_default'):
    """Makes prediction from a saved moel (job_dir) given feeds & fetches tensor names.

    Args:
        inputs (List[str]): Inputs to the model
        job_dir (str): Path to the model/job directory. Could be a GCS or local path.
        feeds (Union[str, List[str]]): Feeds (or input) tensor names.
        fetches (Union[str, List[str]]): Fetches (or output) tensor names.
        structured_outputs (bool): Preserve the structure of the output tensor?
        tags (str, optional): Tags marked by the estimator. Defaults to 'serving_default'.

    Returns:
        np.ndarray or tf.Tensor: Output of the saved model.
    """

    # Load saved model & create a signature def.
    metagraph_def: AutoTrackable = tf.saved_model.load(job_dir)
    signature_def: ConcreteFunction = metagraph_def.signatures[tags]

    # Default feeds.
    feeds = feeds or signature_def.inputs[0].name

    # Default fetches.
    default_fetches: Union[str, List[str], Dict[str, str]] = None
    if structured_outputs:
        # Preserve output structure.
        outputs = signature_def.structured_outputs

        if isinstance(outputs, (list, tuple)):
            default_fetches: List[str] = [t.name for t in outputs]
        elif isinstance(outputs, dict):
            default_fetches: Dict[str, str] = {
                k: v.name for k, v in outputs.items()
            }
        elif isinstance(outputs, tf.Tensor):
            default_fetches: str = outputs.name
        else:
            raise TypeError(f'Unknown output type: {outputs}')
    else:
        # List of outputs.
        default_fetches = [t.name for t in signature_def.outputs]

    fetches = fetches or default_fetches

    # Prune model from feeds (inputs) to fetches (outputs).
    model = metagraph_def.prune(feeds, fetches)
    results = model(inputs)

    return results


def load_data(filename: str, query_string: Optional[str] = None,
              nrows: Optional[int] = None):
    """Load data evaluation data.

    Args:
        filename (str): Path to filename to be evaluated.
        query_string (str, optional): Query string to select a subset of the dataset.
            Defaults to None.
        nrows (int, optional): Limit the number of rows.
            Defaults to None.

    Returns:
        List[str]: List of each rows in filename.
    """
    import pandas as pd

    # Load dataframe from CSV.
    df = pd.read_csv(filename, error_bad_lines=True)
    df.dropna(inplace=True)

    if query_string:
        df = df.query(query_string)
        if len(df) == 0:
            print('Empty dataframe', file=sys.stderr, flush=True)
            exit(1)

    # Get the first `nrows`.
    if nrows is not None:
        df = df.head(nrows)

    # Vin & dealer id with all values in a list.
    # vin_dealerid = df[['vin', 'dealerid']].values
    inputs = [','.join(map(str, val)) for val in df.values]

    return inputs


def run(input_path: str, job_dir: str, **kwargs: Any):
    # Unpack keyword arguments.
    query_string: Optional[str] = kwargs.get('query_string')

    feeds = kwargs.get('feed_tensor_name')
    fetches = kwargs.get('fetch_tensor_name')
    structured_outputs = kwargs.get('structured_outputs')

    # Compare encoded representation with generated representation.
    inputs = load_data(input_path, query_string)

    # Latent / encoded values.
    predcition = predict(tf.constant(inputs), job_dir,
                         feeds=feeds, fetches=fetches,
                         structured_outputs=structured_outputs)
    print(predcition)


def default_args(parser):
    """Provides default values for Workflow flags."""

    parser.add_argument('--input-path', type=str, required=True,
                        help='Input specified as uri to CSV file. Each line of features ')
    parser.add_argument('--job-dir', type=str, required=True,
                        help='GCS location to write checkpoints and export models')

    parser.add_argument('--feed-tensor-name', type=str, nargs='+',
                        help='Feed (or input) tensor name.')
    parser.add_argument('--fetch-tensor-name', type=str, nargs='+',
                        help='Fetch (or output) tensor name.')

    parser.add_argument('--structured-outputs', type=bool,
                        action='store_true', default=True,
                        help='Model\'s outputs are preserved exactly how it was defined.')

    parser.add_argument('--query-string', type=str,
                        help='Query string to extract certain features.')
    parser.add_argument('--nrows', type=int, default=None,
                        help='Number of rows of data to keep.')

    parsed_args, _ = parser.parse_known_args()

    return parsed_args


def main():
    # Parse model arguments.
    arg_dict = default_args(argparse.ArgumentParser())
    run(**vars(arg_dict))


if __name__ == '__main__':
    main()
